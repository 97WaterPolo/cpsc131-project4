#include "ExpressionConverter.h"
#include <stack>
#include <iostream>

using namespace std;

// Write all your functions here

bool isOperator(char c);
int competeOperation(char c, int x, int y);

string Infix2RPN(string s){

	return s;
}
string RPN2Infix(string s){

	return s;
}
ArithNode *Infix2ExpressionTree(string s){
	ArithNode *n = new ArithNode(1);
	return n;
}
ArithNode *RPN2ExpressionTree(string s){
	stack<ArithNode> tree;
	string val = "";
	ArithNode *x, *y;
	int splitter = 0;
	while (s != ""){
		cout << "S Value: " << s << " Val: " << val << endl;
		splitter = s.find_first_of(' ');
		if (splitter == -1){ //Means it is an operator as its the last operation
			x = &tree.top();
			tree.pop();
			y = &tree.top();
			tree.pop();
			ArithNode *t = new ArithNode(val[0]);
			t->right = y;
			if (y->isOperator)
				cout << "ROperator" <<  y->binaryOperator << endl;
			else
				cout << "ROperand" << y->operand << endl;
			t->left = x;
			if (x->isOperator)
				cout << "LOperator" << x->binaryOperator << endl;
			else
				cout << "LOperand" << x->operand << endl;
			return t;
		}
		val = s.substr(0, splitter);
		s = s.substr(splitter + 1);
		if (isOperator(val[0])){
			x = &tree.top();
			tree.pop();
			y = &tree.top();
			tree.pop();
			ArithNode *t = new ArithNode(val[0]);
			t->right = y;
			if (y->isOperator)
				cout << "ROperator" << y->binaryOperator << endl;
			else
				cout << "ROperand" << y->operand << endl;
			t->left = x;
			if (x->isOperator)
				cout << "LOperator" << x->binaryOperator << endl;
			else
				cout << "LOperand" << x->operand << endl;
			tree.push(*t);

		}
		else{
			int i = atoi(val.c_str());
			ArithNode *t = new ArithNode(i);
			tree.push(*t);
		}
	}

	return NULL;
}
int EvaluateInfix(string s){

	return -1;
}
int EvaluateRPN(string s){
	stack<int> rpn;
	string val = "";
	int splitter = 0, x , y;
	while (s != ""){
		splitter = s.find_first_of(' ');
		if (splitter == -1){ //Means it is an operator as its the last operation
			x = rpn.top();
			rpn.pop();
			y = rpn.top();
			rpn.pop();
			return competeOperation(s[0], x, y);
			break;
		}
		val = s.substr(0, splitter);
		s = s.substr(splitter + 1);
		if (isOperator(val[0])){
			x = rpn.top();
			rpn.pop();
			y = rpn.top();
			rpn.pop();
			rpn.push(competeOperation(val[0], x, y));
		}
		else{
			rpn.push(atoi(val.c_str())); //Push the number
		}
	}
	return -1;

}

int competeOperation(char c, int x, int y){
	if (c == '+'){
		return x + y;
	}
	else if (c == '-'){
		return y - x;
	}
	else if (c == '*'){
		return x*y;
	}
	else if (c == '/'){
		return y / x;
	}
}

bool isOperator(char c){
	return (c == '+' || c == '-' || c == '*' || c == '/' );
}